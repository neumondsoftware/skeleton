# Neumond Skeleton

Experimental Skeleton app using Neumond Framework

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Installation

First you need to install dependencies like this:
```shell
composer install
```

## Usage

You can run the project like this:
```shell
php blazar serve
```

Your new app will be running at http://localhost:8000/

As example you can access Framework about page at http://localhost:8000/about

## Contributing

Feel free to create MR with your contributions.

## License

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](./LICENSE)

This project is licensed under the terms of the [MIT license](./LICENSE).
