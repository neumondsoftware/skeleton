<?php

namespace Neumond\Controllers;

use Neumond\Core\Controller;

/**
 * Class HomeController
 * @package Neumond\Controllers
 */
class TestController extends Controller {
    /**
     * Main entry point for this controller
     */
    public function index() {
        $this->view('test');
    }
}
