<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../app/routes.php';

use Neumond\Application;

try {
    $app = new Application($routes);
} catch (Exception $e) {
    echo $e->getMessage();
}
